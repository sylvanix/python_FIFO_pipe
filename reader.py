import os, sys, struct
import numpy as np

imgH, imgW = (512, 640)
imgSz = imgH * imgW
numberFrames = 10
path = "/tmp/imgz.fifo"

try:
    fifo = open(path, 'br')
except Exception as e:
    print(e)
    sys.exit()


for ii in range(numberFrames):
    valBytes = fifo.read(2*imgSz)
    try:
        valInt = struct.unpack(imgSz*'H', valBytes)
        valArr = np.array(valInt).reshape((imgH, imgW))
        print(valArr.shape)
    except:
        break

fifo.close()
