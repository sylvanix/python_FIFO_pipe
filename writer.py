import sys, os, struct, time
import random
import numpy as np

imgH, imgW = (512, 640)
imgSz = imgH * imgW
numberFrames = 10
path = "/tmp/imgz.fifo"

try:
    os.mkfifo(path)
except:
    pass

try:
    fifo = open(path, 'bw')
except Exception as e:
    print(e)
    sys.exit()

for ii in range(numberFrames):
    # some image converted to a Python list
    img = [random.randint(0,255) for _ in range(imgSz)]

    valBytes = struct.pack(imgSz*'H', *img)
    fifo.write(valBytes)

fifo.flush()

print("Closing Server ...")
fifo.close()
try:
    os.unlink(fifo)
except:
    pass
