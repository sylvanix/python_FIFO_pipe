## Inter-process communication using named-pipes.

On POSIX systems, named-pipes can be used for inter-process communication (IPC), meaning that separate threads running on the same system can communicate without involving calls to the network stack, which can be costly in terms of performance. When a named-pipe is created with a call to mkfifo(), the filesystem is used to create a place in memory. Only when both ends of the pipe are opened is the data passed. Unlike regular, un-named pipes, these pipes appear to create a file. Unlike regular files though, these appear to be empty since they are simply a way to access the memory location of the pipe contents using the filesystem.  

In one terminal window enter the following to open one end of the pipe which I will designate "writer":  

```
mkfifo testPipe
echo "IPC is easy with named-pipes.">testPipe
```

Next, in a second terminal window enter the following to open the "reader" end of the pipe:

```
while read line; do echo "Output of testPipe: ${line}"; done<testPipe
```

You could have just as well started the reader before the writer; the process would have waited until the contents appeared in the pipe.  

### A Python implementation  

Python's os module can call the Linux mkfifo() which can then be written to and read from just as if it were a file. Since my work involves images, that is 2D arrays, I'll create some test images and pass those through the pipe. In the following example, I write and read the array values 2-bytes at a time.  

#### The Writer  

```
import sys, os, struct, time
import numpy as np

imgH, imgW = (5, 8)
imgSz = imgH * imgW
path = "/tmp/imgz.fifo"

try:
    os.mkfifo(path)
except:
    pass

try:
    fifo = open(path, 'bw')
except Exception as e:
    print(e)
    sys.exit()

WRITING = True
testVal = 255
while WRITING:
    print('Writing values ...')
    
    for j in range(imgSz):
        value = testVal%256
        valBytes = struct.pack('H', value)
        fifo.write(valBytes)

        if j == (imgSz - 1):
            WRITING = False
            fifo.flush()

print("Closing Server ...")
fifo.close()
try:
    os.unlink(fifo)
except:
    pass
```

#### The Reader  


```
import os, sys, struct
import numpy as np

imgH, imgW = (5, 8)
imgSz = imgH * imgW
path = "/tmp/imgz.fifo"

try:
    fifo = open(path, 'br')
except Exception as e:
    print(e)
    sys.exit()

valArr = []
counter = 0
while True:
    valBytes = fifo.read(2)

    try:
        valInt = struct.unpack('H', valBytes)[0]
        valArr.append(valInt)
    except:
        break

        if  counter == (imgSz - 1):
            READING = False
            fifo.flush()
        counter += 1

    if len(valArr) == imgSz:
        img = np.array(valArr, np.uint8)
        img = img.reshape((imgH, imgW))
        print(img)

fifo.close()
```


Until now I have been buffering camera video frames in a circular buffer written in C++ to read from a Python program running in a separate process. I would like to replace my circular buffer, which uses shared memory, with a named-pipe, as long at it doesn't sacrifice performance or reliability. I would expect the named-pipe implementation to be faster but need to do some comparison testing. In this repository are write.py and read.py which read and write multiple 640x512 arrays of uint-8 values. The write+read operation is slower than I expected, completing only a few array transfers-per-second. When I get more time, I'll try to find some performance gains and then compare transfers-per-second with the the same arrays using the C circular buffer.  
